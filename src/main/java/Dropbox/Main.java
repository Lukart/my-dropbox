package Dropbox;

import Dropbox.listener.DirectoryListener;
import Dropbox.upload.DropBoxUploader;

import java.io.IOException;

public class Main {

    public static void main(String args[]) throws IOException, InterruptedException {
        new DirectoryListener().listen();
    }
}