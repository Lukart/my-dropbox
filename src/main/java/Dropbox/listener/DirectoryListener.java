package Dropbox.listener;

import Dropbox.upload.DropBoxUploader;

import java.io.IOException;
import java.nio.file.*;

public class DirectoryListener {
    private static final String DIR = "C:\\Dropbox\\";
    public void listen() throws InterruptedException, IOException {
        WatchService watchService = FileSystems.getDefault().newWatchService();
        Path path = Paths.get(DIR);
        path.register(
                watchService,
                StandardWatchEventKinds.ENTRY_CREATE);

        WatchKey key;
        while ((key = watchService.take()) != null) {
//            for (WatchEvent<?> event : key.pollEvents()) {
//                System.out.println(
//                        "Event kind:" + event.kind()
//                                + ". File affected: " + event.context() + ".");
//            }
            String name = key.pollEvents().get(0).context().toString();
            System.out.println(name);
            new DropBoxUploader().upload(DIR + name, name);
            key.reset();
        }
    }
}