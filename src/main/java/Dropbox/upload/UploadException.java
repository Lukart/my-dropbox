package Dropbox.upload;

class UploadException extends RuntimeException {
    UploadException(String message, Throwable cause) {
        super(message, cause);
    }
}
