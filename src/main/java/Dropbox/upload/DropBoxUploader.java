package Dropbox.upload;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DropBoxUploader implements Uploader{

    @Override
    public void upload(String path, String name){
        String ACCESS_TOKEN = "NJSofb_sCfAAAAAAAAAAfPiAvzRubSb27VhNr4hO2sQlfiqcnGO2xqq9KupTXglp";
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);
        try (InputStream in = new FileInputStream(path)) {
            client.files().uploadBuilder("/" + name)
                    .uploadAndFinish(in);
        } catch (IOException | DbxException e) {
            throw new  UploadException("Can not upload file " + path, e);
        }
    }
}